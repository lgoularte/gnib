const editFormButton = document.getElementById("btEditDetails");
const submitButton = document.getElementById("btSrch4Apps");
let pollingInterval;
let checkResultInterval;
let polling = false;


// Util functions

function dispachFieldEvent(field, eventName) {
  if ("createEvent" in document) {
    const event = document.createEvent("HTMLEvents");
    event.initEvent(eventName, false, true);
    field.dispatchEvent(event);
  } else {
    field.fireEvent("on"+eventName);
  }
}

function updateFieldAndDispachEvent(selector, value, eventName) {
  const field = document.querySelector(selector);
  field.value = value;
  dispachFieldEvent(field, eventName);
}


// Notifications functions

function beep() {
  const audio = new Audio();
  audio.src = chrome.extension.getURL("siren-noise.mp3");
  audio.play();
}


// Storage functions

function saveFormData(formData) {
  chrome.storage.sync.set({ formData }, function() {
    console.log("Form data saved");
  });  
}

function clearStorage() {
  chrome.storage.sync.clear(function() {
    console.log("Form data removed from storage");
  });
}

// Polling functions

function startPolling() {
  polling = true;
  pollingInterval = setInterval(function() {
    console.log("Polling...");
    dispachFieldEvent(submitButton, "click");
  }, 5000);

  checkPollingResult();
}

function checkPollingResult() {
  checkResultInterval = setInterval(function() {
    console.log("Checking result...");
    const slots = document.querySelectorAll(".appOption");

    if (slots.length) {
      stopPolling();
      beep();
    }
  }, 2000);
}

function stopPolling() {
  clearInterval(pollingInterval);
  clearInterval(checkResultInterval);
  polling = false;
  console.log("Polling stoped");
}


// Form functions

function collectFormData() {
  const data = {};

  document.querySelectorAll('.form-control').forEach(function(field) {
    data[field.id] = field.value;
  });
  return data;
}

function updateFields(data) {
  for (id in data) {
    document.querySelector('#'+id).value = data[id];
  }
}

function fillForm(data) {
  console.log("Filling form...");
  const dependentFieldsData = {};

  for (id in data) {
    switch(id) {
      case 'Category':
      case 'ConfirmGNIB':
      case 'FamAppYN':
      case 'PPNoYN':
        updateFieldAndDispachEvent('#'+id, data[id], "change");
        break;
      case 'AppSelectChoice':
        break;
      default:
        dependentFieldsData[id] = data[id];
        break;
    }
  }

  document.querySelector("[name='UsrDeclaration']").checked = true;

  setTimeout(function() {
    updateFields(dependentFieldsData);
  }, 100);

  setTimeout(function() {
    dispachFieldEvent(document.getElementById("btLook4App"), "click");
  }, 500);

  setTimeout(function() {
    const id = 'AppSelectChoice';
    updateFieldAndDispachEvent('#'+id, data[id], "change");
  }, 1000);
}


// Main function

function main() {
  submitButton.addEventListener("click", function() {
    saveFormData(collectFormData());

    if (!polling) {
      startPolling();
    }
  });

  editFormButton.addEventListener("click", function() {
    stopPolling();
    clearStorage();
  });

  chrome.storage.sync.get('formData', function({ formData }) {
    if (formData) {
      fillForm(formData);
    }
  });
}

main();
